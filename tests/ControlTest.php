<?php
namespace Tests;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;
class ControlTest extends TestCase
{
    public function testIndex()
    {
        $httpClient = HttpClient::create(['base_uri' => 'http://127.0.0.1:8000']);

        $response = $httpClient->request('GET', '/controller/index.php');

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testCreate()
    {
        $httpClient = HttpClient::create(['base_uri' => 'http://127.0.0.1:8000']);

        $response = $httpClient->request('GET', '/controller/create.php');

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testUpdate()
    {
        $httpClient = HttpClient::create(['base_uri' => 'http://127.0.0.1:8000']);

        $response = $httpClient->request('GET', '/controller/update.php?id=3');

        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testDelete()
    {
        $httpClient = HttpClient::create(['base_uri' => 'http://127.0.0.1:8000']);

        $response = $httpClient->request('GET', '/controller/delete.php?id=3', ['max_redirects' => 0]);

        $this->assertEquals($response->getStatusCode(), 302);
    }
}