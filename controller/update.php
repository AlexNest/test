<?php
require_once __DIR__ . "/../model/Article.php";
 date_default_timezone_set("Europe/Kiev");
$id = $_POST['id'];
$row  = ['name' => $_POST['name'], 'description' => $_POST['description']];
if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {
    $article = new Article();
    $article->update($_POST['id'], $_POST['name'], $_POST['description'], $_POST['created_at']);
}
require_once __DIR__ . "/../view/update.php";
