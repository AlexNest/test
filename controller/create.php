<?php

require_once __DIR__ . "/../model/Article.php";
$timestamp = date('Y-m-d H:i:s', time());
date_default_timezone_set("Europe/Kiev");
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {

    $article = new Article();
    $article->insert($_POST['name'], $_POST['description'], $_POST['created_at']);

    header('Location: index.php');
    }

require_once __DIR__ . "/../view/create.php";





